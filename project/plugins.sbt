addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.2")

resolvers += "simplytyped" at "http://simplytyped.github.io/repo/releases"

addSbtPlugin("com.simplytyped" % "sbt-antlr4" % "0.7.5")

