name := "lisa-quickstart"

version := "0.2.2"

organization := "ch.uzh.ifi.seal"

scalaVersion := "2.11.8"

scalacOptions ++= Seq("-Ywarn-unused-import", "-Yinline-warnings", "-feature", "-optimize", "-unchecked", "-deprecation", "-Xlint:-adapted-args")

parallelExecution in Test := false

pollInterval := 1000

antlr4Settings

antlr4PackageName in Antlr4 := Some("org.example.parser")

antlr4GenVisitor in Antlr4 := true

antlr4GenListener in Antlr4 := false

libraryDependencies ++= {
  Seq(
    "org.antlr" % "antlr4" % "4.5.3",
    "lisa-module" % "lisa-module" % "0.2.2" from "https://files.ifi.uzh.ch/seal/lisa/jar/lisa-module-assembly-0.2.2.jar"
  )
}

watchSources <<= (watchSources) map { files =>
  files.filter(f => !f.getName.endsWith(".class"))
}

resolvers ++= Seq(
  "Maven Central Server" at "http://repo1.maven.org/maven2"
)

seq(Revolver.settings: _*)
