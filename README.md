## Notice

This repository serves as a template and demo for how to use and extend LISA.
Visit the [main LISA repository](https://bitbucket.org/sealuzh/lisa) for the
complete source for LISA.

## Synopsis
LISA is a highly generic multi-revision graph database and computation engine
designed for analyzing software artifacts that exist in many revisions of a
project.

It avoids redundancy when analyzing multiple revisions by loading artifacts from
different revisions into a shared representation where only the smallest delta
between revisions requires extra space. Computations also run on entire ranges
of revisions where no change occured at the sub-graph level.

For more information, please refer to [this paper](http://tiny.uzh.ch/Fj)

LISA works on Linux and Mac.

## Quick Start

 * **Make sure you are using Java 8**!
 * Install SBT >= 0.13.12: http://www.scala-sbt.org/download.html
 * Clone the repository and run an example:

```
#!sh

git clone git@bitbucket.org:sealuzh/lisa-quickstart.git
cd lisa-quickstart
sbt 'run-main org.example.LuaAnalysis' # run an example computation
```

If you whish to integrate LISA in your own project, include the following in your build.sbt:

```
libraryDependencies += "lisa-module" % "lisa-module" % "0.2.2" from "https://files.ifi.uzh.ch/seal/lisa/jar/lisa-module-assembly-0.2.2.jar"
```

## Examples

There are two examples contained in `src/main/scala/org/example/`.

### Command line tool for analyzing arbitrary repositories

[AnalyzerCli.scala](https://bitbucket.org/sealuzh/lisa-quickstart/src/master/src/main/scala/org/example/AnalyzerCli.scala) shows how to use the LISA library with configurable parser, analyses and persistence. It uses all built-in parsers and analyses. Analyzing repositories can be done by running something like the following on the command line.

`sbt 'run-main org.example.AnalyzerCli "/tmp/results/lisa-results" "https://github.com/EsotericSoftware/spine-runtimes.git" "309889c65336eab821e8ef695a5ce9ea77069bb2" ""'`

### Adding support for Lua

ANTLR grammars can be dropped in `src/main/antlr4` and they will be automatically built by the [sbt-antlr4](https://github.com/ihji/sbt-antlr4) plugin when running sbt commands. The package prefix for the generated parsers is configured in `built.sbt`.

[LuaAnalysis.scala](https://bitbucket.org/sealuzh/lisa-quickstart/src/master/src/main/scala/org/example/LuaAnalysis.scala) shows what is necessary to support an additional language by adding support for Lua. The ANTLR grammar file, originally taken from the [grammars-v4 repo](https://github.com/antlr/grammars-v4) is located at `src/main/antlr4/Lua.g4`. No modifications were needed except for the addition of [alternative labels](https://theantlrguy.atlassian.net/wiki/display/ANTLR4/Parser+Rules) for different kinds of statements. `LuaAnalysis.scala` contains a mapping defintion for Lua, the boilerplate for wrapping the ANTLR parser, a small analysis for resolving function names and the execution itself. Execute it by running the following command.

`sbt 'run-main org.example.LuaAnalysis'`

To add support for additional languages, the same pattern needs to be followed, i.e. find a suitable grammar file, define a mapping and boilerplate plus analyses as needed.

## Signal/Collect Console

When writing mappings or formulating analyses, it can help to visually explore the ASTs parsed into the graph. To do this, pass `console=true` to `LisaComputation` to enable the Signal/Collect console. The computation will pause when finished and navigating to http://localhost:8080/ loads the console. To end the program, press enter in the terminal.

![Signal/Collect Console](http://i.imgur.com/0xRDI0P.png)

## Further Reading

 * ANTLR: https://theantlrguy.atlassian.net/wiki/display/ANTLR4/Home
 * Signal/Collect: http://www.signalcollect.com/

